class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    word_length = @referee.pick_secret_word
    @board = Array.new(word_length, "_")
    @guesser.register_secret_length(word_length)
  end

  def register_secret_length(word_length)
    @word_length = word_length
  end

  def take_turn
    puts "Guess a letter"
    guessed_letter = @guesser.guess
    ref_reply_indicies = @referee.check_guess(guessed_letter) #spits out array of correct indicies or an empty array if therea are none

    unless ref_reply_indicies.empty?
      update_board(ref_reply_indicies, guessed_letter)
    end

    @guesser.handle_response(guessed_letter, ref_reply_indicies)
  end

  def update_board(idx, guess)
    idx.each {|idx| @board[idx] = guess}
  end

end

class HumanPlayer

  def register_secret_length(word_length)
    @word_length = word_length
  end

  def guess
    gets.chomp
  end
  def handle_response(guess, ref_reply_indicies)

  end



end

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @word = @dictionary.sample
    @word.length
  end

  def register_secret_length(word_length)
    @word_length = word_length
  end

  def check_guess(letter)
    if @word.include?(letter)
      find_indicies(letter)
    else
      []
    end
  end

  def guess(board)
  end

  def find_indicies(letter)
    @word.chars.map.with_index {|ch, i| i if ch == letter}.compact
  end
end

============================
require 'byebug'
class Hangman
  attr_reader :guesser, :referee
  attr_accessor :board, :guesses_left

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
    @guessed_letters = []
    @letters_left = ("a".."z").to_a
  end

  def setup
    word_length = @referee.pick_secret_word
    @board = Array.new(word_length, "_") #sets up the board to be as long as secret word
    @guesser.register_secret_length(word_length)
  end

  def take_turn #is probably a bit long
    puts "Secret word:#{@board.join("")}"
    given_guess = @guesser.guess(@board)
    #manage letters
    @guessed_letters << given_guess
    @letters_left -= [given_guess]
    #get reply from ref
    ref_reply_idxs = @referee.check_guess(given_guess)

    unless ref_reply_idxs.empty?
      update_board(ref_reply_idxs, given_guess)
    end

    @guesser.handle_response(given_guess, ref_reply_idxs)

    # unless @guesser.guesses_left == 1
    #   puts "You have #{@guesser.guesses_left} guesses left."
    # else
    #   puts "Last guess!"
    # end
    puts "You have guessed: #{@guessed_letters.join("")}"
    puts "Letters left: #{@letters_left.join("")}"
  end

  def update_board(idxs, guess)
    idxs.each { |idx| @board[idx] = guess }
  end

  def play
    "Welcome to Hangman!"
    setup
    take_turn until over?
    if board.include?("_")
      puts "Game over."
    else
      puts "Congratulations!"
    end
    puts "The word was #{@referee.word}."
  end

  def over?
    return true if !@board.include?("_")
    return true if @guesser.guesses_left == 0
  end

end

class HumanPlayer
  attr_accessor :guesses_left

  def initialize
  end

  def register_secret_length(word_length)
    @word_length = word_length
    @guesses_left = word_length
  end

  def pick_secret_word
    puts "Ok human, pick a word! Remember it and tell me how long it is:"
    gets.chomp.to_i
  end

  def guess(*blah)
    puts "Take a guess!"
    gets.chomp
  end

  def handle_response(guess, ref_reply_idxs)
    unless ref_reply_idxs.empty?
      puts "Good guess! #{guess} is in the word."
    else
      puts "No dice. #{guess} is not in the word."
      @guesses_left -= 1
    end
    unless @guesses_left == 1
      puts "You have #{@guesses_left} guesses left."
    else
      puts "Last guess!"
    end
  end

  def check_guess(letter)
    puts "The other player guessed #{letter}."
    puts "If that letter is in your word, type the indices of those letters."
    puts "Eg. \"034\""
    puts "If not, must press enter."
    gets.chomp.chars.map(&:to_i)
  end
end

class ComputerPlayer
  attr_reader :word, :candidate_words, :word_length, :guesses_left

  def initialize(dictionary = "dictionary.txt")
    if dictionary == "dictionary.txt"
      @dictionary = File.readlines(dictionary)
    else
      @dictionary = dictionary
    end
  end

  def register_secret_length(word_length)
    @word_length = word_length
    @guesses_left = word_length
    @candidate_words = @dictionary.select { |word| word.length == @word_length }
  end

  def pick_secret_word
    @word = @dictionary.sample.chomp
    @word.length
  end

  def guess(board)
    letter_hash = letter_hashize(@candidate_words, board)
    letter_hash.last.first
  end

  def check_guess(letter)
    if @word.include?(letter)
      return get_indicies(letter)
    else
      []
    end
  end

  def get_indicies(letter)
    @word.chars.map.with_index { |ch, idx| idx if ch == letter }.compact
  end

  def letter_hashize(possible_words, board)
    letter_hash = Hash.new(0)
    possible_words.each do |word|
      word.chars.each do |ch|
        letter_hash[ch] += 1
      end
    end
    letter_hash.delete_if { |ch, num| board.include?(ch) }
    letter_hash.sort_by { |ch, num| num}
  end

  def handle_response(guess, right_idxs)
    # debugger
    unless right_idxs.empty?
      @candidate_words.select! do |word|
        word.chars.each_index do |idx|
          if !right_idxs.include?(idx) && word[idx] == guess
            true
            break
          elsif right_idxs.include?(idx) && word[idx] != guess
            true
            break
          end
        end
      end
    else
      @candidate_words.reject! { |word| word.include?(guess) }
    end
  end
end

if $PROGRAM_NAME == __FILE__
  puts "Select:"
  puts "1) Human v Human"
  puts "2) Human v PC"
  if gets.chomp == "2"
    puts "Press \"g\" to be the guesser, or \"r\" to be the referee."
    if gets.chomp == "g"
      guesser = HumanPlayer.new
      referee = ComputerPlayer.new("dictionary.txt")
    else
      guesser = ComputerPlayer.new("dictionary.txt")
      referee = HumanPlayer.new
    end
  else
    guesser, referee = HumanPlayer.new, HumanPlayer.new
  end
  Hangman.new(guesser: guesser, referee: referee).play
end
